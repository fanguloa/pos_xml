#!/bin/sh

cd /odoo/odoo-server/addons/point_of_sale/static/src/xml/
sudo rm -r pos.xml
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/pos.xml
cd /odoo/odoo-server/addons/point_of_sale/static/src/js/
sudo rm -r models.js
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/models.js
cd /odoo/odoo-server/addons/pos_restaurant/static/src/xml/
sudo rm -r printbill.xml
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/printbill.xml
cd /odoo/odoo-server/addons/pos_restaurant/static/src/xml/
sudo rm -r multiprint.xml
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/multiprint.xml
cd /odoo/odoo-server/addons/point_of_sale/static/src/js/
sudo rm -r screens.js
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/screens.js
cd /odoo/odoo-server/addons/point_of_sale/static/src/js/
sudo rm -r db.js
sudo wget https://gitlab.com/fanguloa/pos_xml/raw/master/db.js
sudo service odoo-server restart